import traceback
import logging
import json
import pymysql
import uuid
import sys
import os

import time

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection
    try:
        Connection = pymysql.connect(os.environ['v_db_host'], os.environ['v_username'], os.environ['v_password'], os.environ['v_database'], connect_timeout=5)
    except Exception as e:
        logger.error(e)
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def fn_CheckBrandExists(lv_CutitronicsBrandID):

    lv_list = ""
    lv_stmt = "SELECT CutitronicsBrandID, BrandName, ContactTitle FROM BrandDetails WHERE CutitronicsBrandID = %(CutitronicsBrandID)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_stmt, {'CutitronicsBrandID': lv_CutitronicsBrandID})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))
            
            if len(lv_list) == 0:
                return 400
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500


def fn_CheckProductExists(lv_CutitronicsBrandID, lv_CutitronicsSKUCode):

    lv_list = ""
    lv_statement = "SELECT CutitronicsBrandID, CutitronicsSKUName, CutitronicsSKUDesc, CutitronicsSKUCode, CutitronicsSKUCat, CutitronicsSKUType, DefaultURL, DefaultIMG, DefaultVideo, DefaultVolume, DefaultParameters, RecommendedAmount FROM BrandProducts WHERE CutitronicsBrandID = %(CutitronicsBrandID)s AND CutitronicsSKUCode = %(CutitronicsSKUCode)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID, 'CutitronicsSKUCode': lv_CutitronicsSKUCode})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))
            
            if len(lv_list) == 0:
                return 400
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500


def fn_updateBrandProduct(lv_CutitronicsBrandID, lv_CutitronicsSKUCode, lv_CutitronicsSKUName, lv_CutitronicsSKUDesc, lv_CutitronicsSKUCat, lv_CutitronicsSKUType, lv_DefaultURL, lv_DefaultIMG, lv_DefaultVideo, lv_DefaultParameters, lv_DefaultVolume, lv_RecommendedAmount, lv_CreationUser):

    lv_statement = "UPDATE BrandProducts SET CutitronicsSKUName = %(CutitronicsSKUName)s, CutitronicsSKUDesc = %(CutitronicsSKUDesc)s, CutitronicsSKUType = %(CutitronicsSKUType)s, CutitronicsSKUCat = %(CutitronicsSKUCat)s, DefaultURL = %(DefaultURL)s, DefaultIMG = %(DefaultIMG)s, DefaultVideo = %(DefaultVideo)s, DefaultVolume = %(DefaultVolume)s, DefaultParameters = %(DefaultParameters)s, RecommendedAmount = %(RecommendedAmount)s WHERE CutitronicsBrandID = %(CutitronicsBrandID)s AND CutitronicsSKUCode = %(CutitronicsSKUCode)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsSKUCode': lv_CutitronicsSKUCode, 'CutitronicsSKUName': lv_CutitronicsSKUName, 'CutitronicsSKUDesc': lv_CutitronicsSKUDesc, 'CutitronicsSKUType': lv_CutitronicsSKUType, 'CutitronicsSKUCat': lv_CutitronicsSKUCat, 'DefaultURL': lv_DefaultURL, 'DefaultIMG': lv_DefaultIMG, 'DefaultVideo': lv_DefaultVideo, 'DefaultVolume': lv_DefaultVolume, 'DefaultParameters': lv_DefaultParameters, 'RecommendedAmount': lv_RecommendedAmount, 'CutitronicsBrandID': lv_CutitronicsBrandID})

            if cursor.rowcount == 1:
                return 100

            elif cursor.rowcount > 1:
                logging.info('Statement rolled back, more than one row affected')
                return 500

            elif cursor.rowcount == 0:
                logging.info('No update')
                return 500
    except pymysql.err.InternalError as e:
        logging.info(e.args)
        return 500

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 500




def lambda_handler(event, context):
    """
    updatePresGuidance

    Creates a new Therapist Session
    """
    openConnection()

    logger.info("Lambda function - {function_name}".format(function_name=context.function_name))

    # Variables

    lv_msgVersion = None
    lv_testFlag = None
    lv_CutitronicsBrandID = None

    # Usage varible
    lv_CutitronicsSKUCode = None
    lv_CutitronicsSKUName = None
    lv_CutitronicsSKUDesc = None
    lv_CutitronicsSKUCat = None
    lv_CutitronicsSKUType = None

    lv_DefaultURL = None
    lv_DefaultIMG = None
    lv_DefaultVideo = None
    lv_DefaultVolume = None
    lv_DefaultParameters = None

    lv_RecommendedAmount = None
    lv_Source = None

    # Return block

    UpdateProduct_out = {"Service": context.function_name, "Status": "Success", "ErrorCode": "", "ErrorDesc": "", "ErrorStack": "", "ServiceOutput": {}}

    try:
        try:
            body = json.loads(event['body'])
        except KeyError as e:
            try:
                body = event
            except TypeError:
                logger.error("The payload is not in the right format")
                app_json = json.dumps(event)
                event = {"body": app_json}
                body = json.loads(event['body'])

        logger.info("Payload body - '{body}'".format(body=body))
        logger.info(" ")
        logger.info("Payload event - '{lv_event}'".format(lv_event=event))

        lv_msgVersion = body.get('msgVersion', 0)
        lv_testFlag = body.get('testFlag', 0)

        # Brand ID from request
        lv_CutitronicsBrandID = body.get('CutitronicsBrandID', '')

        # SKU Details from request
        lv_CutitronicsSKUCode = body.get('CutitronicsSKUCode', '')
        lv_CutitronicsSKUName = body.get('CutitronicsSKUName', '')
        lv_CutitronicsSKUDesc = body.get('CutitronicsSKUDesc', '')
        lv_CutitronicsSKUCat = body.get('CutitronicsSKUCat', '')
        lv_CutitronicsSKUType = body.get('CutitronicsSKUType', '')

        # Default Values from request
        lv_DefaultURL = body.get('DefaultURL', '')
        lv_DefaultIMG = body.get('DefaultIMG', '')
        lv_DefaultVideo = body.get('DefaultVideo', '')
        lv_DefaultVolume = body.get('DefaultVolume', '')
        lv_DefaultParameters = body.get('DefaultParameters', '')

        # Get Usage Variables From Request 
        lv_RecommendedAmount = body.get('RecommendedAmount', '')


        # old vars
        lv_Source = body.get("Source", "")

        '''
        1. Does Brand exist
        '''

        logger.info(" ")
        logger.info("Calling fn_CheckBrandExists to check if the following Brand exists - %s", lv_CutitronicsBrandID)
        logger.info(" ")

        lv_BrandType = fn_CheckBrandExists(lv_CutitronicsBrandID)

        logger.info(" ")
        logger.info("fn_CheckBrandExists return - %s", lv_BrandType)
        logger.info(" ")

        if lv_BrandType == 500 or lv_BrandType == 400:  # Failure

            logger.error("Brand '{lv_CutitronicsBrandID}' has not been found in the back office systems".format(lv_CutitronicsBrandID=lv_CutitronicsBrandID))

            UpdateProduct_out['Status'] = "Error"
            UpdateProduct_out['ErrorCode'] = context.function_name + "_001"  # CreateTherapistSession_001
            UpdateProduct_out['ErrorDesc'] = "Supplied BrandID does not exist in the BO Database"
            UpdateProduct_out["ServiceOutput"]["CutitronicsBrandID"] = lv_CutitronicsBrandID
            UpdateProduct_out["ServiceOutput"]["CutitronicsSKUCode"] = lv_CutitronicsSKUCode            

            # Lambda response
            Connection.close()
            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(UpdateProduct_out)
            }

        '''
        2. Check Product exists
        '''

        logger.info("")
        logger.info("Calling fn_CheckProductExists to check if product '{lv_Product}' exists for brand '{lv_Brand}'".format(lv_Product=lv_CutitronicsSKUCode, lv_Brand=lv_CutitronicsBrandID))
        logger.info("")

        lv_Product  = fn_CheckProductExists(lv_CutitronicsBrandID, lv_CutitronicsSKUCode)

        logger.info(" ")
        logger.info("fn_CheckProductExists return - %s", lv_Product)
        logger.info(" ")

        if lv_Product == 500 or lv_Product == 400:  # Failure

            logger.error("Product '{lv_SKU}' has not been found in the back office systems for brand '{lv_BrandID}'".format(lv_BrandID=lv_CutitronicsBrandID, lv_SKU=lv_CutitronicsSKUCode))

            UpdateProduct_out['Status'] = "Error"
            UpdateProduct_out['ErrorCode'] = context.function_name + "_001"  # CreateTherapistSession_001
            UpdateProduct_out['ErrorDesc'] = "Supplied product does not exist in the BO Database"
            UpdateProduct_out["ServiceOutput"]["CutitronicsBrandID"] = lv_CutitronicsBrandID
            UpdateProduct_out["ServiceOutput"]["CutitronicsSKUCode"] = lv_CutitronicsSKUCode

            # Lambda response
            Connection.close()
            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(UpdateProduct_out)
            }
        

        '''
        3. Update Product Details
        '''

        lv_UpdateProduct = fn_updateBrandProduct(lv_CutitronicsBrandID, lv_CutitronicsSKUCode, lv_CutitronicsSKUName, lv_CutitronicsSKUDesc, lv_CutitronicsSKUCat, lv_CutitronicsSKUType, lv_DefaultURL, lv_DefaultIMG, lv_DefaultVideo, lv_DefaultParameters, lv_DefaultVolume, lv_RecommendedAmount, 'updateBrandProduct')


        if lv_UpdateProduct == 500:  # Failure

            logger.error("Could not update product '{lv_SKU}' in the back office systems".format(lv_SKU=lv_CutitronicsSKUCode))

            UpdateProduct_out['Status'] = "Error"
            UpdateProduct_out['ErrorCode'] = context.function_name + "_001"  # CreateTherapistSession_001
            UpdateProduct_out['ErrorDesc'] = "unable to update product '{lv_SKUCode}' in the back office systems".format(lv_SKUCode=lv_CutitronicsSKUCode)
            UpdateProduct_out["ServiceOutput"]["CutitronicsBrandID"] = lv_CutitronicsBrandID
            UpdateProduct_out["ServiceOutput"]["CutitronicsSKUCode"] = lv_CutitronicsSKUCode

            # Lambda response
            Connection.rollback()
            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(UpdateProduct_out)
            }

        # Create output

        logger.info("")
        logger.info("Completed execution successfully, return: '{updateOut}'".format(updateOut=json.dumps(UpdateProduct_out)))
        logger.info("")

        Connection.commit()
        Connection.close()

        UpdateProduct_out['Status'] = "Success"
        UpdateProduct_out['ErrorCode'] = ""
        UpdateProduct_out['ErrorDesc'] = ""
        UpdateProduct_out["ServiceOutput"]["CutitronicsBrandID"] = lv_CutitronicsBrandID
        UpdateProduct_out["ServiceOutput"]["CutitronicsSKUCode"] = lv_CutitronicsSKUCode

        return {
            "isBase64Encoded": "false",
            "statusCode": 200,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(UpdateProduct_out)
        }

    except Exception as e:
        logger.error("CATCH-ALL ERROR: Unexpected error: '{error}'".format(error=e))

        exception_type, exception_value, exception_traceback = sys.exc_info()
        traceback_string = traceback.format_exception(exception_type, exception_value, exception_traceback)
        err_msg = json.dumps({
            "errorType": exception_type.__name__,
            "errorMessage": str(exception_value),
            "stackTrace": traceback_string
        })
        logger.error(err_msg)

        Connection.rollback()
        Connection.close()

        # Populate Cutitronics reply block

        UpdateProduct_out['Status'] = "Error"
        UpdateProduct_out['ErrorCode'] = context.function_name + "_000"  # CreateTherapistSession_000
        UpdateProduct_out['ErrorDesc'] = "CatchALL"
        
        # Lambda response

        return {
            "isBase64Encoded": "false",
            "statusCode": 500,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(UpdateProduct_out)
        }
